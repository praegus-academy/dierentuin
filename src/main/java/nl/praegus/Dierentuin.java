package nl.praegus;

import java.util.ArrayList;
import java.util.List;

public class Dierentuin {
    private List<Dier> dieren = new ArrayList<>();

    public void voegDierToe(Dier dier) {
        dieren.add(dier);
    }

    /**
     * Een wat korter samengevatte printDierenInfo. Bijzonderheden:
     * - .getClass().getSimpleName() is een functie die de naam van de klasse teruggeeft.
     * Merk ook hier op: ik doe dit op dierentuinDier; wat nog niet gecast is!
     * - System.out.print (zonder ln) print iets op het scherm zonder een nieuwe regel te beginnen.
     * - Ik heb apen ook maar een specifieke eigenschap gegeven; nl het aantal bananen.
     */
    public void printDierenInfo() {
        for (int i = 0; i < dieren.size(); i++) {
            Dier dierentuinDier = dieren.get(i);

            System.out.print("Soort (type): " + dierentuinDier.getClass().getSimpleName() + "; ");
            System.out.print("Naam: " + dieren.get(i).getNaam() + "; ");
            System.out.print("Temperatuur: " + dieren.get(i).getTemperatuur());

            if (dierentuinDier instanceof Hond) {
                System.out.print("; aantal poten: " + ((Hond) dierentuinDier).getAantalPoten());
            }
            if (dierentuinDier instanceof Haai) {
                System.out.print("; aantal vinnen: " + ((Haai) dierentuinDier).getAantalVinnen());
            }
            if (dierentuinDier instanceof Walvis) {
                System.out.print("; aantal vinnen: " + ((Walvis) dierentuinDier).getAantalVinnen());
            }
            if (dierentuinDier instanceof Aap) {
                System.out.print("; aantal bananen: " + ((Aap) dierentuinDier).getAantalBananen());
            }
            System.out.println();
        }
    }

    /**
     * Voorbeeld van een Nullpointer exceptie. Het is natuurlijk een beetje een gekunsteld voorbeeld,
     * in het echt zou je nooit code zo schrijven.
     */
    public static void gooiExceptie() {
        Hond pluto = null;
        try {
            System.out.println("Pluto heeft helemaal geen naam :/ " + pluto.getNaam());
        } catch (NullPointerException e) {
            System.out.println("Exceptie! Melding is: " + e.getMessage());

            pluto = new Hond();
            pluto.setNaam("Flappie");
            System.out.println("Pluto's naam is: " + pluto.getNaam());
        }
    }

    public static void main(String[] args) {
        Dierentuin praegusDierentuin = new Dierentuin();

        praegusDierentuin.voegDierToe(new Hond(Namengenerator.getNaam(), 4, 38.2));
        praegusDierentuin.voegDierToe(new Hond(Namengenerator.getNaam(), 2, 39.5));
        praegusDierentuin.voegDierToe(new Haai(Namengenerator.getNaam(), 9, 20.3));
        praegusDierentuin.voegDierToe(new Walvis(Namengenerator.getNaam(), 3, 38.2));
        praegusDierentuin.voegDierToe(new Aap(Namengenerator.getNaam(), 39.5, 6));

        praegusDierentuin.printDierenInfo();
        System.out.println();
        System.out.println("Walvissen hebben altijd maximaal " +
                Walvis.getMaximumAantalVinnen() + " vinnen");

        gooiExceptie();
    }
}
