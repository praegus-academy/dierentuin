package nl.praegus;

public class Walvis extends Dier {
    private String naam;
    private int aantalVinnen;
    private double temperatuur;
    private static int maximumAantalVinnen;

    public Walvis(String naam, int aantalVinnen, double temperatuur) {
        this.naam = naam;
        this.aantalVinnen = aantalVinnen;
        this.temperatuur = temperatuur;
    }

    public static int getMaximumAantalVinnen() {
        return maximumAantalVinnen;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public int getAantalVinnen() {
        return aantalVinnen;
    }

    public void setAantalVinnen(int aantalVinnen) {
        this.aantalVinnen = aantalVinnen;
    }

    public double getTemperatuur() {
        return temperatuur;
    }

    public void setTemperatuur(double temperatuur) {
        this.temperatuur = temperatuur;
    }
}
