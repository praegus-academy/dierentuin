package nl.praegus;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Dit is een hond. Een hond heeft een aantal poten en heeft longen (hopelijk).
 */
public class Hond extends Dier {
    private int aantalPoten;
    private boolean heeftLongen;

    // in de praktijk heb je natuurlijk nooit 3 lijstjes met hetzelfde; dit is slechts ter illustratie!
    String[] dagelijksRitmeArray = {"eet", "slaapt", "poept", "loopt"};
    List<String> dagelijksRitmeList = Arrays.asList("eet", "slaapt", "poept", "loopt");
    Set<String> dagelijksRitmeSet = new HashSet<>(Arrays.asList(dagelijksRitmeArray));

    public Hond() {
        super();
        aantalPoten = 4;
    }

    /**
     * Instantieer een hond
     * @param naam de naam van de hond
     * @param aantalPoten het aantal poten van een hond (tussen 0 en 4)
     * @param temperatuur graag 35+ (anders is ie dood)
     */
    public Hond(String naam, int aantalPoten, double temperatuur) {
        super(naam, temperatuur);
        this.aantalPoten = aantalPoten;
    }

    public int getAantalPoten() {
        return aantalPoten;
    }

    public void setAantalPoten(int newAantalpoten) {
        this.aantalPoten = newAantalpoten;
    }

    public void amputeerPoot() {
        aantalPoten = aantalPoten - 1;
    }

    /**
     * aantal poten is even of oneven HEB JE DE FUNCTIETITEL WEL GELEZEN? :(
     * @return true of false; even of oneven
     */
    public boolean aantalPotenIsEven() {
        return aantalPoten == 0 || aantalPoten == 2 || aantalPoten == 4;
    }

    public boolean isZiek() {
        if (aantalPoten < 4 || temperatuur > 39.5) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isZiekComplex() {
        if (temperatuur > 35 && temperatuur < 38 && aantalPoten < 4) {
            return true;
        } else if (temperatuur > 38 && temperatuur < 40.362 && aantalPoten == 3) {
            return true;
        }
        return false;
    }

    // bonusopdracht
    public boolean heeftRobotischePootNodig() {
        switch (aantalPoten) {
            case 0:
            case 1:
            case 2:
            case 3:
                return true;
        }
        return false;
    }

    // print ritme met array en while
    public void printRitme() {
        int counter = 0;
        while (counter < dagelijksRitmeArray.length) {
            System.out.println("De hond " + naam + " " + dagelijksRitmeArray[counter]);
            counter = counter + 1;
        }
    }

    // ander voorbeeld van de while loop
    public void printRitmeAndersom() {
        int counter = dagelijksRitmeArray.length - 1;
        while (counter >= 0) {
            System.out.println("De hond " + naam + " " + dagelijksRitmeArray[counter]);
            counter = counter - 1;
        }
    }

    // print ritme met list en for
    public void printRitmeFor() {
        for (int i = 0; i < dagelijksRitmeList.size(); i++) {
            System.out.println("De hond " + naam + " " + dagelijksRitmeList.get(i));
        }
    }

    // print ritme met set en for each
    public void printRitmeForEach() {
        for (String gedrag : dagelijksRitmeSet) {
            System.out.println("De hond " + naam + " " + gedrag);
        }
    }

    // print ritme met array en recursie
    public void printRitmeRecursie(int teller) {
        if (teller >= dagelijksRitmeArray.length) {
            return;
        }
        System.out.println("De hond " + naam + " " + dagelijksRitmeArray[teller]);
        printRitmeRecursie(teller + 1);
    }
}
