package nl.praegus;

public class Aap extends Dier {
    private int aantalBananen;

    public Aap(String naam, double temperatuur, int aantalBananen) {
        super(naam, temperatuur);
        this.aantalBananen = aantalBananen;
    }

    void klim() {
        System.out.println("Aap " + naam + " klimt!");
    }

    public int getAantalBananen() {
        return aantalBananen;
    }
}
