package nl.praegus;

public class Dier {
    String naam;
    double temperatuur;

    public Dier() {}

    public Dier(String naam, double temperatuur) {
        this.naam = naam;
        this.temperatuur = temperatuur;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public double getTemperatuur() {
        return temperatuur;
    }

    public void setTemperatuur(double temperatuur) {
        this.temperatuur = temperatuur;
    }
}
