package nl.praegus;

import java.util.List;

import static java.util.Arrays.asList;

public class Namengenerator {

    private Namengenerator() {
        // static class
    }

    private static List<String> namen = asList("Piet", "Jan", "Klaas", "Marietje", "Sanne", "Magreet");
    private static int teller = 0;

    public static String getNaam() {
        teller++;
        if (teller >= namen.size()) {
            teller = 0;
        }
        return namen.get(teller);
    }
}
