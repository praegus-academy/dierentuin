package nl.praegus;

import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HondTest {

    @Test
    public void een_hond_met_3_poten_is_ziek() {
        Hond hond = new Hond();
        hond.setAantalPoten(3);

        boolean result = hond.isZiek();

        assertThat(result).isTrue();
    }

    @Test
    public void een_hond_met_4_poten_is_niet_ziek() {
        Hond hond = new Hond("Piet", 4, 38.2);

        boolean result = hond.isZiek();

        assertThat(result).isFalse();
    }

    @Test
    public void als_ik_een_poot_amputeer_dan_heeft_ie_een_poot_minder() {
        Hond hond = new Hond("", 4, 0.0);

        hond.amputeerPoot();

        assertThat(hond.getAantalPoten()).isEqualTo(3);
    }

    @Test
    public void een_hond_met_3_poten_heeft_een_robotische_poot_nodig() {
        Hond hond = new Hond(null, 3, 0.0);

        boolean result = hond.heeftRobotischePootNodig();

        assertThat(result).isTrue();
    }
}
