package nl.praegus;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class NamengeneratorTest {

    @Test
    public void namen_worden_opnieuw_uitgegeven() {
        List<String> resultaten = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            resultaten.add(Namengenerator.getNaam());
        }
        assertThat(resultaten).containsExactly("Jan", "Klaas", "Marietje", "Sanne", "Magreet", "Piet", "Jan");
    }
}
